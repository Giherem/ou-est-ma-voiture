package com.jvautard.ouestmavoiture;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LocationListener {
	private LocationManager locationManager;

	private Location currentLocation = null; // Derniere localisation enregistree
	private Location carLocation = null; // Localisation de la voiture.

	private TextView zoneTexte;
	private ImageView zoneImage;
	private Button boutonSet;
	private Button boutonReset;

	private boolean gpsEnabled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		zoneTexte = (TextView) findViewById(R.id.textView1);
		zoneImage = (ImageView) findViewById(R.id.imageView1);
		boutonSet = (Button) findViewById(R.id.buttonSet);
		boutonReset = (Button) findViewById(R.id.buttonReset);

		boutonSet.setOnClickListener(mSetButtonClickListener);
		boutonReset.setOnClickListener(mResetButtonClickListener);

		/********** get Gps location service LocationManager object ***********/
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (OEMVPreferences.sharedInstance().hasLocationStored())
			carLocation = OEMVPreferences.sharedInstance().getLocation();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},1);
		}
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				2000,   // 3 sec
				5, this);
		refreshGraphics();
	}

	@Override
	public void onPause() {
		locationManager.removeUpdates(this);
		super.onPause();
	}

	@Override
	public void onLocationChanged(Location location) {
		gpsEnabled=true;
		currentLocation = location;
		refreshGraphics();
		String str = "Lat: "+location.getLatitude()+"Lon: "+location.getLongitude()+"Acc: "+location.getAccuracy();
	}

	@Override
	public void onProviderDisabled(String provider) {

		/******** Called when User off Gps *********/

		currentLocation = null;
		gpsEnabled = false;
		refreshGraphics();
	}

	@Override
	public void onProviderEnabled(String provider) {

		/******** Called when User on Gps  *********/     
		gpsEnabled = true;
		refreshGraphics();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}



	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}

	View.OnClickListener mSetButtonClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			if (currentLocation == null || currentLocation.getAccuracy() > 60.) {
				AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
				alertBuilder.setTitle("Erreur");
				if (currentLocation != null) {
					String msgComplet = ""+MainActivity.this.getResources().getString(R.string.msgLocPeuPrecisePart1)+currentLocation.getAccuracy()+MainActivity.this.getResources().getString(R.string.msgLocPeuPrecisePart2);
					alertBuilder.setMessage(msgComplet);
				}
				else alertBuilder.setMessage(R.string.msgLocInconnue);
				alertBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				alertBuilder.show();
			}
			else {
				AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
				alertBuilder.setMessage(R.string.confirmEnregNewPosition);
				alertBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setPositionVoiture(currentLocation);
						dialog.dismiss();
					}
				});
				alertBuilder.setNegativeButton("Annuler",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				alertBuilder.show();
			}
		}

	};

	View.OnClickListener mResetButtonClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
			alertBuilder.setMessage(R.string.confirmErasePosition);
			alertBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					setPositionVoiture(null);
					refreshGraphics();
					dialog.dismiss();
				}
			});
			alertBuilder.setNegativeButton("Annuler",new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			alertBuilder.show();
		}

	};

	private void setPositionVoiture(Location l) {
		carLocation = l;
		OEMVPreferences.sharedInstance().storeLocation(carLocation);
		refreshGraphics();
	}

	private void refreshGraphics() {
		this.runOnUiThread(
				new Runnable() {

					@Override
					public void run() {
						if (!gpsEnabled) {
							zoneTexte.setText(R.string.noGPS);
							zoneImage.setImageResource(R.drawable.nogps);
							zoneImage.setRotation(0);
						}
						else if (currentLocation == null) {
							zoneTexte.setText(R.string.waitGPS);							
							zoneImage.setImageResource(R.drawable.gps);
							zoneImage.setRotation(0);
						}
						else if (!OEMVPreferences.sharedInstance().hasLocationStored()) {
							zoneTexte.setText(R.string.noRecordedPosition);
							zoneImage.setImageResource(R.drawable.noposition);
							zoneImage.setRotation(0);
						}
						else {
							float resultat[] = new float[3];
							Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(), carLocation.getLatitude(), carLocation.getLongitude(), resultat);
							float distance = resultat[0];
							float azimutDestination = resultat[2];
							float precision = currentLocation.getAccuracy();
							String aAfficher = ""+distance+"m \n (A "+precision+"m pres)";
							zoneTexte.setText(aAfficher);
							if (precision > 60.) zoneTexte.setTextColor(getResources().getColor(R.color.red));
							else zoneTexte.setTextColor(getResources().getColor(R.color.white));

							// On affiche une flèche verte si on connait la direction de déplacement du gars, rouge sinon.
							// (si pas de direction, la flèche est orientée par rapport au nord)
							if (currentLocation.hasBearing()) zoneImage.setImageResource(R.drawable.fleche);
							else zoneImage.setImageResource(R.drawable.flecherouge);
							zoneImage.setRotation(azimutDestination-currentLocation.getBearing());
						}
					}

				}
				);
	}
}
