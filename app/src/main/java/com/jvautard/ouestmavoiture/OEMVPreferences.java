package com.jvautard.ouestmavoiture;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;


public class OEMVPreferences {

	private static final String nomFichier = "Preferences";
	
	private static final String PREFS_LONGITUDE = "longitude";
	private static final String PREFS_LATITUDE = "latitude";
	private static final String PREFS_ACCURACY = "precision";
	
	private static final String defValue = "0.0";
	
	private SharedPreferences prefs=null;
	
	private OEMVPreferences() {
		prefs = OEMVApplication.getContext().getSharedPreferences(nomFichier, 0);
	}
	
	private static OEMVPreferences _instance=null;
	
	public static OEMVPreferences sharedInstance() {
		if (_instance == null) _instance = new OEMVPreferences();
		return _instance;
	}
	
	public void storeLocation(Location loc) {
		Editor edit = prefs.edit();
		if (loc == null) {
			edit.putString(PREFS_LATITUDE, defValue);
			edit.putString(PREFS_LONGITUDE,defValue);
			edit.putString(PREFS_ACCURACY,defValue);
			
		} else {
			edit.putString(PREFS_LATITUDE, ""+loc.getLatitude());
			edit.putString(PREFS_LONGITUDE,""+loc.getLongitude());
			edit.putString(PREFS_ACCURACY,""+loc.getAccuracy());
		}
		edit.commit();
	}
	
	public Location getLocation() {
		Location loc = new Location("OEMVPrefs");
		loc.setLongitude(Double.parseDouble(prefs.getString(PREFS_LONGITUDE, defValue)));
		loc.setLatitude(Double.parseDouble(prefs.getString(PREFS_LATITUDE, defValue)));
		loc.setAccuracy(Float.parseFloat(prefs.getString(PREFS_ACCURACY, defValue)));
		return loc;
	}
	
	public boolean hasLocationStored() {
		return !(prefs.getString(PREFS_LONGITUDE,defValue).equals(defValue));
	}
	
}
