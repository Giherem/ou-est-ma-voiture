package com.jvautard.ouestmavoiture;
import android.app.Application;
import android.content.Context;


public class OEMVApplication extends Application {


	static private Context appContext;

	public static Context getContext() {return appContext;}

	@Override
	public void onCreate() {
		super.onCreate();
		appContext = this.getApplicationContext();
	}

}
